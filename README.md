# EZproxy Playbook

## Purpose
This repository manages two files, `config.txt` and `shibuser.txt`, while providing read-only access for Collection Services.

## How This Works
Members of DUL DevOps are able to run an `Ansible` playbook that will deploy the two files to their correct VMs.

## VMs Served By The Playbook
### Production
* `lib-alumni-ezproxy-01.lib.duke.edu` -- aka `alumniproxy.lib.duke.edu`
* `lib-ezproxy-dku-01.lib.duke.edu1` -- aka `dkuproxy.lib.duke.edu`

### Staging
* `lib-ezproxy-test-01.lib.duke.edu` -- aka `proxy-test.libbray.duke.edu`
* `lib-alumni-ezproxy-test-01.lib.duke.edu` -- aka `alumniproxy-test.lib.duke.edu`
* `lib-ezproxy-dku-test-01.lib.duke.edu1` -- aka `dkuproxy-test.lib.duke.edu`

## Where To Look For Files

`templatse/ezproxy/self_managed` is the starting folder where `config.txt` and `shibuser.txt` reside.  
  
You can find the DUL (main) instance files under:  
* `templates/ezproxy/self_managed/config.txt.j2`
* `templates/ezproxy/self_managed/shibuser.txt.j2`
  
  
For the Alumni instance:  
* `templates/ezproxy/self_managed/alumni/config.txt.j2`
* `templates/ezproxy/self_managed/alumni/shibuser.txt.j2`
  
  
Finally, for the DKU instance:  
* `templates/ezproxy/self_managed/dku/config.txt.j2`
* `templates/ezproxy/self_managed/dku/shibuser.txt.j2`
  
  
## Variables
A common set of variables is available at `group_vars/ezproxy/self_managed/main.yml`.
